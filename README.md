# Agenda

### routes

J'ai essayé d'implémenter Swagger, mais ça ne fonctionne pas. Par manque de temps, description des routes ici :

    GET /contact/{id}

retourne le contact qui a cet id en format json

    GET /contacts
retourne tous les contacts en format json

    GET /users
retourne tous les users en format json

    POST /users
body :
    '{
        "name": string,
        "favoriteColor": string
    }'

créé un nouvel user et le retourne en format json, ou jette une erreur.