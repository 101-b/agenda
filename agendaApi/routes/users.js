var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    res.send('{ "res": "respond with a list of users" }');
});

router.get(
    '/:id',
    (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        res.send(`{ "res" : "respond with user with id = ${req.params.id}" }`)
    })

router.post(
    '/',
    (req, res) => {
        if (req.body.name && req.body.favoriteColor)
        {

        res.setHeader('Content-Type', 'application/json');
        res.send(`{
            "name" : ${req.body.name},
            "favoriteColor": ${req.body.favoriteColor}
            }`
        );
        } else {
            throw new Error('body must have "name": string and "favoriteColor": string defined');
        }

})
module.exports = router;
