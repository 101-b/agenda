var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    res.send('{ "res": "respond with a list of contacts" }');
});

router.get(
    '/:id',
    (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        res.send(`{ "res" : "respond with contact with id = ${req.params.id}" }`);
})

module.exports = router;
